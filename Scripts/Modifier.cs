﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttributeSystem
{
    public class Modifier
    {
        public uint Id { get; private set; }
        public ModifierType ModifierType { get; private set; }
        public BaseAttribute Attribute { get; private set; }

        #region Constructors

        public Modifier() { }

        public Modifier(uint id, ModifierType modifierType, BaseAttribute attribute)
        {
            Id = id;
            ModifierType = modifierType;
            Attribute = attribute;
        }

        public Modifier(Modifier modifier)
        {
            Id = modifier.Id;
            ModifierType = modifier.ModifierType;
            Attribute = modifier.Attribute;
        }

        public Modifier Clone()
        {
            return new Modifier(this);
        }

        #endregion
    }
}
