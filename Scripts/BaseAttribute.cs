﻿namespace AttributeSystem
{
    [System.Serializable]
    public abstract class BaseAttribute
    {
        public float Value { get; private set; }
        public float MinValue { get; private set; }
        public float MaxValue { get; private set; }
        public float IncreaseStep { get; private set; }
        public ModifierType Modifier { get; private set; }

        public string Name { get { return GetType().Name; } }

        public void Init(float value, float minValue, float maxValue, float increaseStep, ModifierType modifier)
        {
            Value = value;
            MinValue = minValue;
            MaxValue = maxValue;
            IncreaseStep = increaseStep;
            Modifier = modifier;
        }

        public BaseAttribute () { }

        public virtual BaseAttribute Clone()
        {
            return null;
        }

        public BaseAttribute(BaseAttribute attribute)
        {
            Value = attribute.Value;
            MinValue = attribute.MinValue;
            MaxValue = attribute.MaxValue;
            IncreaseStep = attribute.IncreaseStep;
            Modifier = attribute.Modifier;
        }

    }
}
