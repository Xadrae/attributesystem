﻿using System;

namespace AttributeSystem
{
    public enum ModifierType
    {
        Addition,
        Percentage
    }
}