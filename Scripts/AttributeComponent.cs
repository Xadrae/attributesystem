﻿using AttributeSystem;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttributeSystem
{
    public abstract class AttributeComponent
    {
        public BaseAttribute attribute { get; private set; }
        public float Value { get; private set; }

    }
}
